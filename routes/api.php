<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/register', 'UserController@register');
Route::post('/login', 'UserController@login');

Route::group(['middleware' => 'auth.jwt'], function() {
    Route::post('/logout', 'UserController@logout');
    Route::post('/courses/register', 'CourseController@register');
    Route::get('/courses', 'CourseController@index');
    Route::get('/courses/export/{type}', 'CourseController@export');
    Route::get('/courses/create', 'CourseController@create');

});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
